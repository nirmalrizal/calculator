import React, { useState, useEffect, useRef } from "react";

// components
import CalculatorButton from "./CalculatorButton";

// constants
const validKeys = [
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "0",
  ".",
  "+",
  "-",
  "*",
  "/",
  "%",
  "(",
  ")",
  "Enter"
];

const Calculator = () => {
  const [input, setInput] = useState("0");
  const inputRef = useRef("0");
  const [output, setOutput] = useState("");
  const [calculationPerformed, setCalculationPerformed] = useState(false);

  useEffect(() => {
    window.addEventListener("keypress", e => {
      const key = e.key;
      if (validKeys.includes(key)) {
        if (key === "Enter") {
          handleButtonClick("=");
        } else {
          setTimeout(() => {
            handleButtonClick(key);
          }, 0);
        }
      }
    });
  }, []);

  const handleButtonClick = key => {
    if (key === "=") {
      const endingWithSymbol = /[+\-*/%]$/g.test(input);
      if (!endingWithSymbol) {
        try {
          const result = eval(inputRef.current);
          setOutput(`${inputRef.current}=`);
          updateInputValues(result);
          setCalculationPerformed(true);
        } catch (err) {
          console.log(err);
        }
      }
    } else if (key === "AC") {
      if (calculationPerformed) {
        setCalculationPerformed(false);
      }
      setOutput("");
      updateInputValues("0");
    } else {
      if (calculationPerformed) {
        setCalculationPerformed(false);
      }
      if (inputRef.current === "0" || calculationPerformed) {
        updateInputValues(key);
      } else {
        updateInputValues(`${inputRef.current}${key}`);
      }
    }
  };

  const updateInputValues = value => {
    inputRef.current = value;
    setInput(value);
  };

  return (
    <div className="row">
      <div className="col calculator-wrapper">
        <div className="calculator-card">
          <div className="row">
            <div className="col output-card">
              <div className="output">
                <small>{output}</small>
              </div>
              <div className="input">{input}</div>
            </div>
          </div>

          <div className="row">
            <CalculatorButton value="(" onClick={handleButtonClick} />
            <CalculatorButton value=")" onClick={handleButtonClick} />
            <CalculatorButton value="%" onClick={handleButtonClick} />
            <CalculatorButton value="AC" onClick={handleButtonClick} />
          </div>
          <div className="row">
            <CalculatorButton
              type="light"
              value="7"
              onClick={handleButtonClick}
            />
            <CalculatorButton
              type="light"
              value="8"
              onClick={handleButtonClick}
            />
            <CalculatorButton
              type="light"
              value="9"
              onClick={handleButtonClick}
            />
            <CalculatorButton value="/" onClick={handleButtonClick} />
          </div>
          <div className="row">
            <CalculatorButton
              type="light"
              value="4"
              onClick={handleButtonClick}
            />
            <CalculatorButton
              type="light"
              value="5"
              onClick={handleButtonClick}
            />
            <CalculatorButton
              type="light"
              value="6"
              onClick={handleButtonClick}
            />
            <CalculatorButton value="*" onClick={handleButtonClick} />
          </div>
          <div className="row">
            <CalculatorButton
              type="light"
              value="1"
              onClick={handleButtonClick}
            />
            <CalculatorButton
              type="light"
              value="2"
              onClick={handleButtonClick}
            />
            <CalculatorButton
              type="light"
              value="3"
              onClick={handleButtonClick}
            />
            <CalculatorButton value="-" onClick={handleButtonClick} />
          </div>
          <div className="row">
            <CalculatorButton
              type="light"
              value="0"
              onClick={handleButtonClick}
            />
            <CalculatorButton
              type="light"
              value="."
              onClick={handleButtonClick}
            />
            <CalculatorButton
              type="primary"
              value="="
              onClick={handleButtonClick}
            />
            <CalculatorButton value="+" onClick={handleButtonClick} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Calculator;
