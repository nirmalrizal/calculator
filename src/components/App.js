import React from "react";

// components
import Calculator from "./Calculator";

function App() {
  return (
    <div className="container">
      <Calculator />
    </div>
  );
}

export default App;
