import React from "react";

const CalculatorButton = ({ value, type, onClick }) => {
  return (
    <div className="calculator-button-wrapper">
      <button
        onClick={() => onClick(value)}
        className={`btn calculator-button 
        ${type === "primary" && "btn-primary"} 
        ${type === "light" && "btn-light"} 
        ${typeof type === "undefined" && "btn-secondary"}`}
      >
        {value}
      </button>
    </div>
  );
};

export default CalculatorButton;
